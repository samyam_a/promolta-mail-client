var q = require('q');
var _ = require('lodash');
const jwt = require('node.jwt')

exports.authenticated = function(req, res, next){
    var config = req.app.get('config');

    auth_cookie = _.get(req.cookies,_.get(config,'auth_cookie'));
    if(!auth_cookie){
        res.redirect('/login');
        return;
    }
    var decoded = jwt.decode(auth_cookie, _.get(config,'token_secret'))
    req.user = _.get(decoded,'payload');

    if(_.get(req.user, 'user_id')){
        req.view_data = {user: req.user};
        next();
    }
    else{
        res.redirect('/login');
        return;
    }
};