module.exports = [
    ['/', 'index#index','auth#authenticated','get'],
    ['/inbox', 'index#inbox','auth#authenticated','get'],
    ['/sent', 'index#sent','auth#authenticated','get'],
    ['/drafts', 'index#drafts','auth#authenticated','get'],
    ['/trash', 'index#trash','auth#authenticated','get'],
    ['/view/:msg_id', 'index#viewMessage','auth#authenticated','get'],
    ['/read/:msg_id', 'api#readMail','auth#authenticated','get'],
    ['/compose', 'api#createNewMail','auth#authenticated','post'],
    ['/edit/:msg_id', 'api#getMailForEdit','auth#authenticated','get'],
    ['/send/:msg_id', 'api#sendMail','auth#authenticated','post'],
    ['/updateMail', 'api#updateMail','auth#authenticated','post'],
    ['/reply', 'api#replyToMail','auth#authenticated','post'],
    ['/forward', 'api#forwardMail','auth#authenticated','post'],

    ['/login', 'index#login','get'],
    ['/login', 'index#dologin','post'],
    ['/logout', 'index#logout','get'],
];