var gulp = require('gulp');
var gutil = require('gulp-util');
var runSequence = require('run-sequence');
var bower = require('bower');
var mainBowerFiles = require('main-bower-files');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var es = require('event-stream');
var clc = require('cli-color');
var uglify = require('gulp-uglify');
var nodemon = require('gulp-nodemon');
var gulpsync  = require('gulp-sync')(gulp);

var runTimestamp = Math.round(Date.now() / 1000);

var jsRoot = './ui/';
var sassRoot = './sass/';
var bowerRoot = "./bower_components/";

var destRoot = './public/';
var destJs = destRoot + 'javascripts/';
var destCss = destRoot + 'stylesheets/';
var destAssets = destRoot + 'assets/';

var paths = {
    scripts: [
        jsRoot + '**/*.js',
    ],
    sass: [
        sassRoot + '**/*.scss'
    ],
};

gulp.task('default-builds', ['sass', 'scripts', 'publish'], function(done){
    done();
});

gulp.task('default', function (done) {
    runSequence('pre-publish', 'default-builds', function () {
        done();
    });
});

// gulp.task('serve', gulpsync.sync(['default', 'watch', 'nodemon']));
gulp.task('serve',function(done) {
    runSequence('default', 'watch', 'nodemon', function() {
        done();
    });
});
    
gulp.task('styles:sass', function (done) {
    gulp.src(paths.sass)
        .pipe(sass()
            .on('error', function (err) {
                console.log(clc.red.bold(err.toString()));
                this.emit('end');
            })
    )
        .pipe(concat('app.css'))
        .pipe(gulp.dest(sassRoot))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest(destCss))
        .on('end', done)
    ;
});

gulp.task('assets:common', function () {
    gulp.src(mainBowerFiles("**/*.*"), { base: bowerRoot })
        .pipe(gulp.dest(destAssets));
});

gulp.task('scripts:app', function () {
    disc_js = gulp.src(paths.scripts)
        .pipe(concat('app.js'))
        .pipe(gulp.dest(destJs))
        .pipe(uglify())
        .on('error', onError)
        .pipe(rename({extname: '.min.js'}))
        .pipe(gulp.dest(destJs));

    return disc_js;

});

gulp.task('scripts', ['scripts:app']);
gulp.task('sass', ['styles:sass']);

gulp.task('watch', function (done) {
    gulp.watch(paths.sass, ['styles:sass']);
    gulp.watch(paths.scripts, ['scripts:app']);
    done();
});

gulp.task('install', ['git-check'], function () {
    return bower.commands.install()
        .on('log', function (data) {
            gutil.log('bower', gutil.colors.cyan(data.id), data.message);
        });
});

gulp.task('git-check', function (done) {
    if (!sh.which('git')) {
        console.log(
            '  ' + gutil.colors.red('Git is not installed.'),
            '\n  Git, the version control system, is required to download Ionic.',
            '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
            '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
        );
        process.exit(1);
    }
    done();
});

gulp.task('pre-publish', function() {
    return gulp.src(mainBowerFiles("**/*.scss"), { base: bowerRoot })
        // .pipe(rename(function(path){
        //     if(path.basename[0] !== "_") path.basename = "_"+path.basename;
        // }))
        .pipe(gulp.dest(sassRoot));
});

gulp.task('nodemon', function (cb) {
    
    var started = false;
    
    return nodemon({
        script: './bin/www',
        exec: 'node --inspect --debug=5733',
        ignore: ["ui/*.js", "README"] 
    }).on('start', function () {
        // to avoid nodemon being started multiple times
        // thanks @matthisk
        if (!started) {
            cb();
            started = true; 
        } 
    });
});

gulp.task('publish', ['assets:common']);

function onError(err) {
    gutil.log(err);
    this.emit('end');
}