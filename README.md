# Installation Instructions 

Please run on nodeV8 LTS (8.9.x)

Ensure npm is atleast v6.0.x (`npx` command is available in this version)

Run the server on the same machine (uses port 3000).

`npm install`

`npx bower install`

`npx gulp serve`

Client is available on port 8080.