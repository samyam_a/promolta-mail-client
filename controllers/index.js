var _ = require('lodash');
var q = require('q');
var Client = require('node-rest-client').Client;
const jwt = require('node.jwt')
const MailServer = require('../services/mail-server');


function viewMessage(req, res, next) {
  var msg_id = _.get(req.params, 'msg_id')

  _.merge(req.view_data, {
    title: "Inbox - Promolto Mail Test",
    mail_box_id: null
  });
  MailServer.getMailForView(msg_id)
  .then(function(mail){
    _.set(req.view_data,'mail', mail)
    res.render('view-message', req.view_data); 
  })
}

function __renderListingPage(req, res){
  MailServer.getMailsForUser(req.user.user_id, req.view_data.mail_box_id)
  .then(function(mails){
    _.set(req.view_data,'mails', mails)
    res.render('index', req.view_data); 
  })
}

function index(req, res, next) {
  _.merge(req.view_data, {
    title: "Inbox - Promolto Mail Test",
    mail_box_id: 1
  });
  __renderListingPage(req, res);
}

function drafts(req, res, next){
  _.merge(req.view_data, {
    title: "Drafts - Promolto Mail Test",
    mail_box_id: 3
  });
  __renderListingPage(req, res);
}

function sent(req, res, next){
  _.merge(req.view_data, {
    title: "Sent Mail - Promolto Mail Test",
    mail_box_id: 2
  });
  __renderListingPage(req, res);
}

function trash(req, res, next){
  _.merge(req.view_data, {
    title: "Trash - Promolto Mail Test",
    mail_box_id: 4
  });
  __renderListingPage(req, res);
}

function login(req, res){
    res.render('login', {title: 'Promolta Mail test app'})
}

function __verifyUser(email, password){
    var defer = q.defer();
    client = new Client();
    var args = {
      data: { email: email, password: password },
      headers: { "Content-Type": "application/json" }
    };
    client.post('http://localhost:3000/users/validate', args, function(data, response){
      if(response.statusCode == 200){
        defer.resolve(data);
      }else{
        defer.reject();
      }
    })
    return defer.promise;
}

function __setLoginCookie(req, res, user){
  var config = req.app.get('config');
  var payload = _.pick(user, ['first_name', 'last_name', 'email']);
  var user_id = _.get(user, 'id');
  if(user_id){
    _.set(payload, 'user_id', user_id)
    var auth_cookie = jwt.encode(payload, _.get(config,'token_secret'));
    res.cookie(_.get(config, 'auth_cookie'), auth_cookie, {httpOnly: true});
  }
}

function __clearLoginCookie(req, res){
  var config = req.app.get('config');
  res.cookie(_.get(config, 'auth_cookie'), '', {httpOnly: true});
}

function dologin(req, res){
    var body = req.body;
    var email = _.get(body,'email');
    var password = _.get(body,'password');
    __verifyUser(email, password)
    .then(function(user){
      __setLoginCookie(req, res, user);
      res.redirect('/');
    })
    .catch(function(error){

    });
}

function logout(req, res){
  __clearLoginCookie(req,res);
  res.redirect('/');
}

exports = module.exports = {
    index: index,
    inbox: index,
    sent: sent,
    drafts: drafts,
    trash: trash,
    login: login,
    dologin: dologin,
    logout: logout,
    viewMessage: viewMessage,
};