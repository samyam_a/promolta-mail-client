var _ = require('lodash');
var q = require('q');
const MailServer = require('../services/mail-server');

exports.createNewMail = function(req, res){
  MailServer.createNewMail(req.user.user_id, null, false)
  .then(function(message){
    res.json(message);
  })
};

exports.replyToMail = function(req, res){
  var parent_id = _.get(req.body,'parent_id');
  MailServer.createNewMail(req.user.user_id, parent_id, false)
  .then(function(message){
    res.json(message);
  })
};

exports.forwardMail = function(req, res){
  var parent_id = _.get(req.body,'parent_id');
  MailServer.createNewMail(req.user.user_id, parent_id, true)
  .then(function(message){
    res.json(message);
  })
};

exports.getMailForEdit = function(req,res){
  var msg_id = _.get(req.params,'msg_id');
  MailServer.getMailForEdit(msg_id)
  .then(function(message){
    res.json(message);
  })
}
exports.sendMail = function(req,res){
  var msg_id = _.get(req.params,'msg_id');
  MailServer.sendMail(msg_id)
  .then(function(message){
    res.json(message);
  })
}

exports.readMail = function(req,res){
  var msg_id = _.get(req.params,'msg_id');
  MailServer.readMail(req.user.user_id, msg_id)
  .then(function(message){
    res.send(200,'{}');
  })
}

exports.updateMail = function(req, res){
  var id = _.get(req.body,'id');
  var mail_to = _.get(req.body,'mail_to');
  var subject_line = _.get(req.body,'subject_line');
  var body = _.get(req.body,'body');
  MailServer.updateMail(req.user.user_id, 
    {
      id: id,
      mail_to: mail_to,
      subject_line: subject_line,
      body: body
    }
  )
  .then(function(message){
    res.json(message);
  })
};