
$(function(){
    console.log("Init: Promolta")
    window.composeMailData = null;
})

function composeNewMail(){
  $.ajax('/compose',{
    method: "POST",
    contentType: 'application/json',
  }).done(function(data){
    window.composeMailData = data;
    $('#composeModal').modal('show');
  });  
}

function composeReplyToMail(mail_id){
  $.ajax('/reply',{
    method: "POST",
    contentType: 'application/json',
    data: JSON.stringify({parent_id: mail_id})
  }).done(function(data){
    window.composeMailData = data;
    $('#composeModal').modal('show');
  });  
}

function composeForwardMail(mail_id){
  $.ajax('/forward',{
    method: "POST",
    contentType: 'application/json',
    data: JSON.stringify({parent_id: mail_id})
  }).done(function(data){
    window.composeMailData = data;
    $('#composeModal').modal('show');
  });  
}

function editMail(mail_id){
  $.ajax('/edit/'+mail_id,{
    method: "GET",
    contentType: 'application/json',
  }).done(function(data){
    window.composeMailData = data;
    $('#composeModal').modal('show');
  });  
}

function readMail(mail_id){
  $.ajax('/read/'+mail_id,{
    method: "GET",
    contentType: 'application/json',
  }).done(function(data){
  });  
}

function viewMail(mail_id){
  window.location="/view/"+mail_id;
}

function __syncComposeData(){
  window.composeMailData.mail_to = $('#inputComposeMailTo').val();
  window.composeMailData.subject_line = $('#inputComposeSubject').val();
  window.composeMailData.body = $('#inputComposeBody').val();
}
function __clearComposeUI(){
  $('#inputComposeMailTo').val('');
  $('#inputComposeSubject').val('');
  $('#inputComposeBody').val('');
}

function sendMail(){
  __syncComposeData();
  if(window.composeMailData.id){
    $.ajax({
      url:'/updateMail',
      method: "POST",
      contentType: 'application/json',
      data: JSON.stringify(window.composeMailData)
    }).done(function(data){
      var mail_id = window.composeMailData.id
      $.ajax({
        url:'/send/'+mail_id,
        method: "POST",
        contentType: 'application/json',
      }).done(function(){
        $('#composeModal').modal('hide');
      })
      window.composeMailData = {};
    });
  }
}


$('#composeModal').on('hide.bs.modal', function (e) {
  __syncComposeData();
  if(window.composeMailData.id){
    $.ajax({
      url:'/updateMail',
      method: "POST",
      contentType: 'application/json',
      data: JSON.stringify(window.composeMailData)
    }).done(function(data){
      window.composeMailData = {};
      __clearComposeUI();
    });
  }
  
});

$('#composeModal').on('shown.bs.modal', function (e) {
  $('#inputComposeMailTo').val(window.composeMailData.mail_to);
  $('#inputComposeSubject').val(window.composeMailData.subject_line);
  $('#inputComposeBody').val(window.composeMailData.body);
});