var _ = require('lodash');
var q = require('q');
var Client = require('node-rest-client').Client;
const config = require('../config');

function __getServerURL(suffix){
  var host = _.get(config, "server.host");
  return host+suffix;
}

exports.getMailsForUser = function(user_id, box_id, page=1, count=20){
  var defer = q.defer();
    client = new Client();
    var args = {
      data: { user_id: user_id, box_id: box_id, page:page,count:count },
      headers: { "Content-Type": "application/json" }
    };
    client.post('http://localhost:3000/messages', args, function(data, response){
      if(response.statusCode == 200){
        defer.resolve(data);
      }else{
        defer.reject();
      }
    })
    return defer.promise;
};

exports.createNewMail = function(user_id, parent_id, is_forward){
  var defer = q.defer();
    client = new Client();
    var args = {
      data: { user_id: user_id, parent_id: parent_id, is_forward: is_forward },
      headers: { "Content-Type": "application/json" }
    };
    client.post('http://localhost:3000/messages/create', args, function(data, response){
      if(response.statusCode == 200){
        defer.resolve(data);
      }else{
        defer.reject();
      }
    })
    return defer.promise;
};

exports.updateMail = function(user_id, data){
  var defer = q.defer();
    client = new Client();
    var args = {
      data: data,
      headers: { "Content-Type": "application/json" }
    };
    client.post('http://localhost:3000/messages/update', args, function(data, response){
      if(response.statusCode == 200){
        defer.resolve(data);
      }else{
        defer.reject();
      }
    })
    return defer.promise;
};

exports.getMailForEdit = function(mail_id){
  var defer = q.defer();
    client = new Client();
    var args = {
      headers: { "Content-Type": "application/json" }
    };
    client.get('http://localhost:3000/messages/'+mail_id, args, function(data, response){
      if(response.statusCode == 200){
        defer.resolve(data);
      }else{
        defer.reject();
      }
    })
    return defer.promise;
};

exports.getMailForView = function(mail_id){
  var defer = q.defer();
    client = new Client();
    var args = {
      headers: { "Content-Type": "application/json" }
    };
    client.get('http://localhost:3000/messages/'+mail_id+'/view', args, function(data, response){
      if(response.statusCode == 200){
        defer.resolve(data);
      }else{
        defer.reject();
      }
    })
    return defer.promise;
};

exports.updateMail = function(user_id, data){
  var defer = q.defer();
    client = new Client();
    var args = {
      data: data,
      headers: { "Content-Type": "application/json" }
    };
    client.post('http://localhost:3000/messages/update', args, function(data, response){
      if(response.statusCode == 200){
        defer.resolve(data);
      }else{
        defer.reject();
      }
    })
    return defer.promise;
};

exports.sendMail = function(msg_id){
  var defer = q.defer();
    client = new Client();
    var args = {
      headers: { "Content-Type": "application/json" }
    };
    client.post('http://localhost:3000/messages/'+msg_id+'/send', args, function(data, response){
      if(response.statusCode == 200){
        defer.resolve(data);
      }else{
        defer.reject();
      }
    })
    return defer.promise;
};

exports.readMail = function(user_id, msg_id){
  var defer = q.defer();
    client = new Client();
    var args = {
      headers: { "Content-Type": "application/json" },
      data: {user_id:user_id}
    };
    client.post('http://localhost:3000/messages/'+msg_id+'/read', args, function(data, response){
      if(response.statusCode == 200){
        defer.resolve(data);
      }else{
        defer.reject();
      }
    })
    return defer.promise;
};