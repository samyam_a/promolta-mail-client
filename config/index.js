var _   = require('lodash'),
defaults = {};

_.set(defaults, 'token_secret', "ThisIsNotASecret");
_.set(defaults, 'auth_cookie', "bp");
_.set(defaults, 'server.host', "http://localhost:3000");

function __getEnvConfig(){
    switch(process.env.NODE_ENV){
        case 'production':
            return require('./lib/prod');
        case 'staging':
            return require('./lib/staging');
        case 'development':
            return require('./lib/dev');
        default:
            return {};
    }
}
console.log("Your environment : ",  process.env.NODE_ENV);

config = {};
var env = __getEnvConfig();
var final = _.defaultsDeep(config,env,defaults);
console.log(final);
exports = module.exports = final;